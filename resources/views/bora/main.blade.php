<!DOCTYPE html>
<html>
<head>
	<title>Impulse - Responsive HTML5 Template</title>

	<!-- Meta -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
    
	<!-- Favicons -->
	<link rel="shortcut icon" href="{{ theme_url('assets/images/favicon.ico') }}"  />
	<link rel="apple-touch-icon" href="{{ theme_url('assets/images/apple-touch-icon.png') }}" />
	<link rel="apple-touch-icon" sizes="72x72" href="{{ theme_url('assets/images/apple-touch-icon-72x72.png') }}" />
	<link rel="apple-touch-icon" sizes="114x114" href="{{ theme_url('assets/images/apple-touch-icon-114x114.png') }}" />
	<link rel="apple-touch-icon" sizes="144x144" href="{{ theme_url('assets/images/apple-touch-icon-144x144.png') }}" />
	
	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{ theme_url('assets/css/style.css') }}"  />
	<link id="impulse_ui" rel="stylesheet" href="{{ theme_url('assets/css/themes/red/red.css') }}"  /> 
	
	<script src="{{ theme_url('assets/plugins/jquery/jquery.js') }}" ></script>
	<script src="{{ theme_url('assets/plugins/jquery-ui/js/jquery-ui.min.js') }}" ></script>
	<script src="{{ theme_url('assets/plugins/bootstrap/js/bootstrap.min.js') }}" ></script>
	<script src="{{ theme_url('assets/plugins/superfish/js/superfish.js') }}" ></script>
	<script src="{{ theme_url('assets/plugins/isotope/isotope.pkgd.min.js') }}" ></script>
	<script src="{{ theme_url('assets/plugins/sidr/js/jquery.sidr.min.js') }}" ></script>

	<script src="{{ theme_url('assets/js/functions.js') }}" ></script>
	<script src="{{ theme_url('assets/js/theme_options.js') }}" ></script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js" ></script>
</head>
<body>


<!-- HEADER -->
<div id="header" class="v1">
 
    <div id="nav_bar">
        <div class="container">
            <div class="row">
			
                <div class="col-md-4">
                    <a class="logo" href="{{ url('/') }}"><img src="{{ theme_url('assets/images/logo.png') }}" alt="" /></a>
                </div>
				
                <div class="col-md-8">
                    <ul id="nav" class="sf-menu">
						<li><a href="index.html">HOME</a></li>
                        <li>
							<a href="about.html">FEATURES</a>
							<ul>
								<li>
									<a href="header1.html">HEADERS</a>
									<ul>
										<li><a href="header1.html">HEADER V1</a></li>
										<li><a href="header2.html">HEADER V2</a></li>
										<li><a href="header3.html">HEADER V3</a></li>
									</ul>
								</li>
								<li><a href="about.html">ABOUT US</a></li>
								<li><a href="services.html">SERVICES</a></li>
								<li><a href="team.html">TEAM</a></li>
								<li><a href="pricing.html">PRICING TABLES</a></li>
								<li><a href="shortcodes.html">SHORTCODES</a></li>
								<li>
									<a href="portfolio-item.html#">THIRD LEVEL ITEM</a>
									<ul>
										<li><a href="portfolio-item.html#">THIRD</a></li>
										<li><a href="portfolio-item.html#">LEVEL</a></li>
										<li><a href="portfolio-item.html#">ITEM</a></li>
									</ul>
								</li>
							</ul>
						</li>
                        <li class="active">
							<a href="portfolio3.html">PORTFOLIO</a>
							<ul>
								<li><a href="portfolio2.html">2 COLUMNS</a></li>
								<li><a href="portfolio3.html">3 COLUMNS</a></li>
								<li><a href="portfolio4.html">4 COLUMNS</a></li>
								<li><a href="portfolio-item.html">PORTFOLIO ITEM</a></li>
							</ul>
						</li>
                        <li>
							<a href="blog.html">BLOG</a>
							<ul>
								<li><a href="blog.html">BLOG LAYOUT</a></li>
								<li><a href="blog-post.html">BLOG POST</a></li>
							</ul>
						</li>
                        <li>
							<a href="shop.html">SHOP</a>
							<ul>
								<li><a href="shop.html">SHOP LISTING</a></li>
								<li><a href="shop-product.html">SHOP PRODUCT</a></li>
								<li><a href="shop-cart.html">SHOPPING CART</a></li>
								<li><a href="shop-wishlist.html">SHOP WISHLIST</a></li>
								<li><a href="shop-compare.html">SHOP COMPARE</a></li>
							</ul>
						</li>
                        <li><a href="contact.html">CONTACT</a></li>
                    </ul>
					
                </div>
				
				<a id="mobile_menu" href="portfolio-item.html#sidr"><i class="fa fa-bars fa-2x"></i></a>
				<div id="sidr">
					<a class="sidr_close" href="portfolio-item.html#"><i class="fa fa-times-circle"></i></a>
					<ul>
						<li><a href="index.html">Home</a></li>
						<li class="groupheader">
							<a href="about.html">Features</a>
							<span class="fa fa-angle-right headerbutton"></span>
							<ul>
								<li class="groupheader">
									<a href="header1.html">Headers</a>
									<span class="fa fa-angle-right headerbutton"></span>
									<ul>
										<li><a href="header1.html">Header V1</a></li>
										<li><a href="header2.html">Header V2</a></li>
										<li><a href="header3.html">Header V3</a></li>
									</ul>
								</li>
								<li><a href="about.html">About Us</a></li>
								<li><a href="services.html">Services</a></li>
								<li><a href="team.html">Team</a></li>
								<li><a href="pricing.html">Pricing Tables</a></li>
								<li><a href="shortcodes.html">Shortcodes</a></li>
								<li class="groupheader">
									<a href="portfolio-item.html#">Third Level Item</a>
									<span class="fa fa-angle-right headerbutton"></span>
									<ul>
										<li><a href="portfolio-item.html#">Third</a></li>
										<li><a href="portfolio-item.html#">Level</a></li>
										<li><a href="portfolio-item.html#">Item</a></li>
									</ul>
								</li>
							</ul>
						</li>
						
						<li class="groupheader">
							<a href="portfolio3.html">Portfolio</a>
							<span class="fa fa-angle-right headerbutton"></span>
							<ul>
								<li><a href="portfolio2.html">2 Columns</a></li>
								<li><a href="portfolio3.html">3 Columns</a></li>
								<li><a href="portfolio4.html">4 Columns</a></li>
								<li><a href="portfolio-item.html">Portfolio Item</a></li>
							</ul>
						</li>
						
						<li class="groupheader">
							<a href="blog.html">Blog</a>
							<span class="fa fa-angle-right headerbutton"></span>
							<ul>
								<li><a href="blog.html">Blog Listing</a></li>
								<li><a href="blog-post.html">Blog Post</a></li>
							</ul>
						</li>
						
						<li class="groupheader">
							<a href="shop.html">Shop</a>
							<span class="fa fa-angle-right headerbutton"></span>
							<ul>
								<li><a href="shop.html">Shop Listing</a></li>
								<li><a href="shop-product.html">Shop Product</a></li>
								<li><a href="shop-cart.html">Shopping Cart</a></li>
								<li><a href="shop-wishlist.html">Shop Wishlist</a></li>
								<li><a href="shop-compare.html">Shop Compare</a></li>
							</ul>
						</li>
						
						<li><a href="contact.html">Contact</a></li>
					</ul>
				</div>
				
            </div>
        </div>
    </div>

</div>
<!-- /HEADER -->

@yield('body')
	
	

<!-- FOOTER -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="contact_us col-md-3">
                <div class="heading">GET IN TOUCH</div>
             </div>

            <div class="hot_tags col-md-3">
                <div class="heading">HOT TAGS</div>
                <a href="portfolio-item.html#">PHP</a>
                <a href="portfolio-item.html#">MySQL</a>
                <a href="portfolio-item.html#">WordPress</a>
                <a href="portfolio-item.html#">CMS</a>
                  <a href="portfolio-item.html#">eCommerce</a>
                 <a href="portfolio-item.html#">LESS</a>

            </div>

            <div class="latest_tweets col-md-3">
                <div class="heading">LATEST TWEETS</div>
                <div class="twitter_feed"></div>
            </div>

            <div class="flickr_photos col-md-3">
                <div class="heading">FLICKR PHOTOS</div>
                <ul id="flickr_feed">
                    <li style="display:none">&nbsp;</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /FOOTER -->

<!-- SUB FOOTER -->
<div id="sub_footer">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-5">
                Copyright &copy; 2017 All rights reserved
            </div>
            <div class="menu col-md-7 col-sm-7">
                <ul>
                    <li><a href="portfolio-item.html#">Home</a></li>
                    <li><a href="portfolio-item.html#">About</a></li>
                    <li><a href="portfolio-item.html#">Services</a></li>
                    <li><a href="portfolio-item.html#">Portfolio</a></li>
                    <li><a href="portfolio-item.html#">Blog</a></li>
                    <li><a href="portfolio-item.html#">Shop</a></li>
                    <li><a href="portfolio-item.html#">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /SUB FOOTER -->

</body>
</html>