@extends('main')

@section('body')
<!-- FEATURED BLOCK -->
<div id="featured_block">
    <div class="container">
        <div class="row">
           <div class="col-md-8">
				<h2>Portfolio Item</h2>
			</div>
			<div class="col-md-4">
				<ul class="breadcrumb pull-right">
					<li class="home"><i class="fa fa-home"></i> <a href="index.html">Home</a></li>
					<li><a href="portfolio3.html">Portfolio</a></li>
					<li class="active"><a href="portfolio-item.html#">Details</a></li>
				</ul>
			</div>
        </div>
    </div>
</div>
<!-- /FEATURED BLOCK -->

<!-- PORTFOLIO ITEM PAGE -->
<div id="portfolio_item" class="page">

    <div class="container">
        <div class="row">

			<div class="project_description col-md-4">
				<h1 class="block_title">OVERVIEW</h1>
					<a class="fancybox" href="{{ url('/go') }}"><img width="100%" class="preview" src="{{ url('/image/'.$json->editionId.'/'.str_slug($json->editionName)) }}/460/595"></a>



				<div class="share">
					<span class='st_facebook_hcount' displayText='Facebook'></span>
					<span class='st_twitter_hcount' displayText='Tweet'></span>
					<span class='st_pinterest_hcount' displayText='Pinterest'></span>

				</div>
			</div>


			<div class="project_details col-md-4">
				<h3 class="block_title">CATEGORIES</h3>
				<table>
				@foreach ($categories->data as $category)
					<tr>
						<td class="title"><a class="title" href="{{ url('/category/'.$category->category_id.'/'.str_slug($category->name)) }}">{{ strtoupper($category->name) }}</a></td>
					</tr>
				@endforeach
				</table>
			</div>
			<div class="project_details col-md-4">
				<h3 class="block_title">DETAILS</h3>

				<table>
					<tr>
						<td class="title"><i class="fa fa-user"></i> CLIENT:</td>
						<td>Company, Inc.</td>
					</tr>
					<tr class="odd">
						<td class="title"><i class="fa fa-calendar"></i> DATE:</td>
						<td>January 17, 2015</td>
					</tr>
					<tr>
						<td class="title"><i class="fa fa-tags"></i> CATEGORIES:</td>
						<td>Web Design, UI Design, Graphics</td>
					</tr>
					<tr class="odd">
						<td class="title"><i class="fa fa-tasks"></i> TECHNOLOGIES:</td>
						<td>PHP, MySQL, HTML5, CSS3, jQuery, Photoshop</td>
					</tr>
					<tr>
						<td class="title"><i class="fa fa-globe"></i> WEBSITE:</td>
						<td><a href="portfolio-item.html#">www.company.com</a></td>
					</tr>
				</table>
			</div>
			<div class="space_40"></div>

			<div class="col-md-12">
				<h3 class="block_title">OLD ISSUE</h3>
			</div>

				<div class="related_works col-md-12 no_padding_left no_padding_right">

					@foreach ($related as $rel)
					<div class="col-md-2">
						<div class="item">
							<a href="{{ url('/'.str_slug($rel->editionId)) }}/{{ str_slug($rel->editionName) }}.html"><img src="{{ url('/image/'.$rel->editionId.'/'.str_slug($rel->editionName)) }}/232/300" alt="" /></a>
							<div class="details">
								<h2><a href="{{ url('/'.str_slug($rel->editionId)) }}/{{ str_slug($rel->editionName) }}.html">{{ $rel->magName. ' - '.$rel->editionName }}</a></h2>
							</div>
						</div>
					</div>
					@endforeach

				</div>




        </div>
    </div>
    <div class="space_40"></div>

</div>
<!-- /PORTFOLIO ITEM PAGE -->

@endsection
