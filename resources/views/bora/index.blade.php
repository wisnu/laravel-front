@extends('main')

@section('body')
<!-- FEATURED BLOCK -->
<div id="featured_block">
    <div class="container">
        <div class="row">
			<div class="col-md-8">
				<h2>Portfolio 4Columns</h2>
			</div>
			<div class="col-md-4">
				<ul class="breadcrumb pull-right">
					<li class="home"><i class="fa fa-home"></i> <a href="index.html">Home</a></li>
					<li class="active"><a href="portfolio4.html#">Portfolio</a></li>

				</ul>
			</div>
        </div>
    </div>
</div>
<!-- /FEATURED BLOCK -->

<!-- PORTFOLIO 4COLUMNS -->
<div id="portfolio4" class="page portfolio">

    <div class="container">

		<div class="row">
 		</div>

        <div class="row">

            <div class="portfolio_items">

                <!-- 1st row -->
                @foreach ($data->data as $data)
				<div class="col-md-2">
                    <div class="project">
						<a href="{{ url('/'.str_slug($data->editionId)) }}/{{ str_slug($data->editionName) }}.html"><img src="{{ url('/image/'.$data->editionId.'/'.str_slug($data->editionName)) }}/232/300" alt="" /></a>
						<div class="details">
							<h2><a href="{{ url('/'.str_slug($data->editionId)) }}/{{ str_slug($data->editionName) }}.html">{{ $data->magName. ' - '.$data->editionName }}</a></h2>
						</div>
					</div>
                </div>

                @endforeach
				<!-- /2nd row -->

            </div>

			<div class="space_40"></div>
			<ul class="pagination">
				<li><a class="prev" href="portfolio4.html#"><i class="fa fa-caret-left"></i> Prev</a></li>
				<li><a href="portfolio4.html#">1</a></li>
				<li class="active"><a href="portfolio4.html#">2</a></li>
				<li><a href="portfolio4.html#">3</a></li>
				<li><a href="portfolio4.html#">4</a></li>
				<li><a class="next" href="portfolio4.html#">Next <i class="fa fa-caret-right"></i></a></li>
			</ul>

        </div>
    </div>

</div>
<!-- /PORTFOLIO 4COLUMNS -->

@endsection