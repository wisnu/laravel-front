<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function index() {
		$json = json_decode(file_get_contents('http://pool.swaukm.com/magazine/30'));
		return view('index')
		->with('data', $json);
		//   echo json_encode($data);
	}


	public function getitem($id) {
		$json = json_decode($this->item($id));
		$related = json_decode(file_get_contents('http://pool.swaukm.com/magazine/mag/'.$json->mid.'/6'));
		$categories = json_decode(file_get_contents('http://pool.swaukm.com/magazine/categories'));

		return view('item')
		->with('json', $json)
		->with('related', $related)
		->with('categories', $categories)
		;

	}


	public function item($id) {
		return file_get_contents("http://pool.swaukm.com/magazine/get/$id");
	}


	public function remoteImg($id, $slug=null, $w=null, $h=null) {
		$item = json_decode($this->item($id));
		Image::configure(array('driver' => 'gd'));
		return Image::make($item->highRes)->resize($w, $h)->response('jpg');
		//  return $item;

	}


}
