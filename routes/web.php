<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'Controller@index');

Route::get('{id}/{slug}.html', 'Controller@getitem');
Route::get('image/{id}/{slug?}/{w?}/{h?}', 'Controller@remoteImg')
// ->where('slug', '[1-9]+[0-9]*')
// ->where('w', '[1-9]+[0-9]*')
// ->where('h', '[1-9]+[0-9]*')
;
Route::get('category/{id}/{slug}', 'Controller@category');
Route::get('go', function(){
		return Redirect::to(Config::get('money.afflink'), 301);
	});
